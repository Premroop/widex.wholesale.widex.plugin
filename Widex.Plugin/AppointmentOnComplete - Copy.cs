﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using Microsoft.Xrm.Sdk.Query;

namespace Widex.Plugin
{
    public class AppointmentOnComplete : IPlugin
    {

        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService =
                (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)
                serviceProvider.GetService(typeof(IPluginExecutionContext));

            tracingService.Trace("Plugin started: Inside execute method");
            Entity entity = new Entity();
            // The InputParameters collection contains all the data passed 
            // in the message request.
            if (context.InputParameters.Contains("Target") &&
                context.InputParameters["Target"] is Entity)
            {
                // Obtain the target entity from the Input Parameters.
                tracingService.Trace
                    ("Getting the target entity from Input Parameters.");
                entity = (Entity)context.InputParameters["Target"];
            }
            else
            {
                if (context.InputParameters["Target"] is EntityReference)//In case of a delete operation
                {
                    entity.LogicalName = ((EntityReference)context.InputParameters["Target"]).LogicalName;
                    entity.Id = ((EntityReference)context.InputParameters["Target"]).Id;
                }
            }

            // Obtain the image entity from the Pre Entity Images.
            tracingService.Trace
                ("Getting image entity from PreEntityImages.");

            Entity preImage = context.PreEntityImages.Count > 0 ? (Entity)context.PreEntityImages["PreImage"] : null;

            // Verify that the target entity represents a contact.
            // If not, this plug-in was not registered correctly.
            tracingService.Trace
                ("Verifying that the target entity represents an appointment.");
            if (entity.LogicalName != "appointment")
                return;

            try
            {
                tracingService.Trace("Entity Id:{0}", context.PrimaryEntityId);

                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = serviceFactory.CreateOrganizationService(null);//Running under the context of CRM system user
                IOrganizationService serviceUserContext = serviceFactory.CreateOrganizationService(context.InitiatingUserId);

                FillFieldsOnAccountOrContact(entity, preImage, service, serviceUserContext);

                tracingService.Trace("Plugin execution completed.");
            }

            catch (FaultException<OrganizationServiceFault> ex)
            {
                tracingService.Trace("The application terminated with an error.");
                tracingService.Trace("Timestamp: {0}", ex.Detail.Timestamp);
                tracingService.Trace("Code: {0}", ex.Detail.ErrorCode);
                tracingService.Trace("Message: {0}", ex.Detail.Message);
                tracingService.Trace("Inner Fault: {0}",
                    null == ex.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault");
            }
            catch (System.TimeoutException ex)
            {
                tracingService.Trace("The application terminated with an error.");
                tracingService.Trace("Message: {0}", ex.Message);
                tracingService.Trace("Stack Trace: {0}", ex.StackTrace);
                tracingService.Trace("Inner Fault: {0}",
                    null == ex.InnerException.Message ? "No Inner Fault" : ex.InnerException.Message);
            }
            catch (System.Exception ex)
            {
                tracingService.Trace("The application terminated with an error.");
                tracingService.Trace(ex.Message);

                // Display the details of the inner exception.
                if (ex.InnerException != null)
                {
                    tracingService.Trace(ex.InnerException.Message);

                    FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> fe = ex.InnerException
                        as FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>;
                    if (fe != null)
                    {
                        tracingService.Trace("Timestamp: {0}", fe.Detail.Timestamp);
                        tracingService.Trace("Code: {0}", fe.Detail.ErrorCode);
                        tracingService.Trace("Message: {0}", fe.Detail.Message);
                        tracingService.Trace("Trace: {0}", fe.Detail.TraceText);
                        tracingService.Trace("Inner Fault: {0}",
                            null == fe.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault");
                    }
                }
            }

        }

        /// <summary>
        /// Checks if an appointment has a <see cref="Contact"/> set as the regardingobjectid and whether or not this contact needs to be updated with information
        /// from the appointment.
        /// </summary>
        /// <param name="target">The target of the appointment.</param>
        /// <param name="preImage">The Pre-Image of the appointment</param>
        /// <seealso cref="FillNextOpenAppointmentFields"/>
        /// <seealso cref="FillLatestCompletedAppointmentFields"/>
        private void FillFieldsOnAccountOrContact(Entity target, Entity preImage, IOrganizationService service, IOrganizationService serviceUserContext)
        {
            //Create collection to store Guid's of related contacts
            var contactIds = new List<Guid>();
            var accountIds = new List<Guid>();

            #region Collect ID's from RegardingObjectId

            if (target != null && target.Contains("regardingobjectid"))
            {
                switch (((EntityReference)target["regardingobjectid"]).LogicalName ?? "")
                {
                    case "account":
                        accountIds.Add(((EntityReference)target["regardingobjectid"]).Id);
                        break;
                    case "contact":
                        contactIds.Add(((EntityReference)target["regardingobjectid"]).Id);
                        break;
                    default:
                        break;
                }
            }

            // Read regarding from preImage
            if (preImage != null && preImage.Contains("regardingobjectid"))
            {
                switch (((EntityReference)preImage["regardingobjectid"]).LogicalName ?? "")
                {
                    case "account":
                        accountIds.Add(((EntityReference)preImage["regardingobjectid"]).Id);
                        break;
                    case "contact":
                        contactIds.Add(((EntityReference)preImage["regardingobjectid"]).Id);
                        break;
                    default:
                        break;
                }
            }
            #endregion

            #region Process contacts
            // Do we have ID's pointing at contact records
            if (contactIds.Any(id => id != Guid.Empty))
            {
                // Iterate an update all related records
                foreach (Guid contactId in contactIds.Distinct())
                {
                    // Create a Contact
                    Entity contact = new Entity("contact");
                    Entity account = new Entity("account");
                    contact.Id = contactId;

                    // Fill data for latest completed appointment into the contact
                    bool contactWasChanged = FillLatestCompletedAppointmentFields(contact, service);

                    // Fill data for next open appointment into the contact
                    contactWasChanged = FillNextOpenAppointmentFields(contact, service) || contactWasChanged;

                    if (contactWasChanged == true)
                    {
                        serviceUserContext.Update(contact);

                        if (contact.Contains("parentcustomerid")) //parent customer id is added to the contact entity inside FillLatestCompletedAppointmentFields() method.
                        {
                            account.Id = ((EntityReference)contact["parentcustomerid"]).Id;
                            //get account lastvisit date field.
                            account = service.Retrieve("account", account.Id, new ColumnSet("wid_lastsalesvisitdate"));
                            bool updateAccount = false;
                            if (account.Contains("wid_lastsalesvisitdate"))
                            {
                                if (contact.Contains("wid_lastsalesvisitdate"))
                                {
                                    //if (((DateTime)account["wid_lastsalesvisitdate"]).Date <= ((DateTime)contact["wid_lastsalesvisitdate"]).Date)
                                    //{

                                    //    account["wid_lastsalesvisitdate"] = contact["wid_lastsalesvisitdate"];

                                    //    if (contact.Contains("wid_nextopenappointment"))
                                    //    {
                                    //        account["wid_nextopenappointment"] = contact["wid_nextopenappointment"];
                                    //    }
                                    //    if (contact.Contains("wid_nextsalesvisitdate"))
                                    //    {
                                    //        account["wid_nextsalesvisitdate"] = contact["wid_nextsalesvisitdate"];
                                    //    }
                                    //    serviceUserContext.Update(account);
                                    //}
                                    // Fill data for latest completed appointment into the contact
                                    bool accountWasChanged = FillLatestCompletedAppointmentFields(account, service);

                                    // Fill data for next open appointment into the contact
                                    accountWasChanged = FillNextOpenAppointmentFields(account, service) || accountWasChanged;

                                    if (accountWasChanged == true)
                                    {
                                        serviceUserContext.Update(account);
                                    }
                                }
                                else
                                {
                                    //if there are no appointments for contacts then check if any closed appointments for account and associated contacts then set the last visit date accordingly.

                                    // Fill data for latest completed appointment into the contact
                                    bool accountWasChanged = FillLatestCompletedAppointmentFields(account, service);

                                    // Fill data for next open appointment into the contact
                                    accountWasChanged = FillNextOpenAppointmentFields(account, service) || accountWasChanged;

                                    if (accountWasChanged == true)
                                    {
                                        serviceUserContext.Update(account);
                                    }
                                }
                            }
                            else
                            {
                                if (contact.Contains("wid_lastsalesvisitdate"))
                                {
                                    account["wid_lastsalesvisitdate"] = contact["wid_lastsalesvisitdate"];
                                    updateAccount = true;
                                }
                                if (contact.Contains("wid_nextopenappointment"))
                                {
                                    account["wid_nextopenappointment"] = contact["wid_nextopenappointment"];
                                    updateAccount = true;
                                }
                                if (contact.Contains("wid_nextsalesvisitdate"))
                                {
                                    account["wid_nextsalesvisitdate"] = contact["wid_nextsalesvisitdate"];
                                    updateAccount = true;
                                }
                                if (updateAccount)
                                {
                                    serviceUserContext.Update(account);
                                }
                            }
                        }
                    }
                    else
                    {
                        //
                        // Log.Info("FillFieldsOnContact(Appointment, Appointment)",
                        // String.Format("No need for an update of the contact set as regarding on appointment with ID '{0}'...", target.Id));
                    }
                }
            }
            #endregion

            //#region Process Accounts
            //// Do we have ID's pointing at account records
            if (accountIds.Any(id => id != Guid.Empty))
            {
                // Iterate an update all related records
                foreach (Guid accountId in accountIds.Distinct())
                {
                    // Create an Account
                    Entity account = new Entity("account");
                    account.Id = accountId;

                    // Fill data for latest completed appointment into the contact
                    bool accountWasChanged = FillLatestCompletedAppointmentFields(account, service);

                    // Fill data for next open appointment into the contact
                    accountWasChanged = FillNextOpenAppointmentFields(account, service) || accountWasChanged;

                    if (accountWasChanged == true)
                    {
                        serviceUserContext.Update(account);
                    }
                    else
                    {
                        //
                        //Log.Info("FillFieldsOnContact(Appointment, Appointment)",
                        //    String.Format("No need for an update of the account set as regarding on appointment with ID '{0}'...", target.Id));
                    }
                }
            }
            //#endregion

        }

        /// <summary>
        /// <para>Fills data into wid_lastsalesvisitdate on a given account/contact.</para>
        /// <para>Data is taken from a completed customer facing appointment that has the newest scheduledstart date.</para>
        /// </summary>
        /// <param name="entity">The account/contact into which data should be filled.</param>
        /// <returns><c>true</c> when data has to be filled into the account/contact; otherwise, <c>false</c></returns>
        private bool FillLatestCompletedAppointmentFields(Entity entity, IOrganizationService service)
        {
            // Ensure we have a contact with a valid ID
            if (entity != null && Guid.Empty != entity.Id)
            {
                // Store the current number of attributes in the contact
                var currentNumberOfAttributesInContact = entity.Attributes.Count;

                var accountIdandContactIds = new List<string>();

                // Build query to retrieve the completed appointment 
                var query = new QueryExpression("appointment");
                query.ColumnSet.AddColumns("activityid", "scheduledstart", "wid_appointmenttypeid");
                query.Criteria.AddCondition("statecode", ConditionOperator.Equal, 1);
                if (entity.LogicalName == "account")//if regardingobject is account then need to get all the associated contacts appointments
                {
                    accountIdandContactIds.Add(entity.Id.ToString());
                    //get all contact ids.
                    var QEcontact = new QueryExpression("contact");
                    // get contact ids
                    QEcontact.ColumnSet.AddColumns("contactid");
                    // Define filter QEcontact.Criteria
                    QEcontact.Criteria.AddCondition("parentcustomerid", ConditionOperator.Equal, entity.Id);
                    QEcontact.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);
                    EntityCollection contacts = service.RetrieveMultiple(QEcontact);
                    for (int i = 0; i < contacts.Entities.Count; i++)
                    {
                        accountIdandContactIds.Add(contacts.Entities[i].Id.ToString());//Add all the assoiciated contact Ids
                    }
                    query.Criteria.AddCondition("regardingobjectid", ConditionOperator.In, accountIdandContactIds.ToArray());//Here all the assoicated contact ids of account is passed along with account id
                }
                else
                {
                    query.Criteria.AddCondition("regardingobjectid", ConditionOperator.Equal, entity.Id.ToString());
                }
                //query.Criteria.AddCondition("scheduledstart", ConditionOperator.LessEqual, DateTime.Now);//avoid future dates for completed appointments. This is handled at appointment level so commenting it.
                query.AddLink("wid_appointmenttype", "wid_appointmenttypeid", "wid_appointmenttypeid")
                    .LinkCriteria.AddCondition("wid_facetoface", ConditionOperator.Equal, true);
                //
                // Add link to Contact -or- Account
                if (entity.LogicalName == "contact")
                    query.AddLink("contact", "regardingobjectid", "contactid")
                        .Columns.AddColumns("wid_lastsalesvisitdate", "parentcustomerid");//"ap_latestappointmenttypeid". //get parentcustomerid(account id) from the related contact
                else if (entity.LogicalName == "account")//if it is account then we have to check for associated contacts as well.
                {
                    query.AddLink("account", "regardingobjectid", "accountid", JoinOperator.LeftOuter)
                        .Columns.AddColumns("wid_lastsalesvisitdate");// "ap_latestappointmenttypeid"
                    query.AddLink("contact", "regardingobjectid", "contactid", JoinOperator.LeftOuter)
                            .Columns.AddColumns("wid_lastsalesvisitdate");
                }

                query.AddOrder("scheduledstart", OrderType.Descending);
                query.NoLock = true;
                query.Distinct = false;
                query.TopCount = 1;
                // Set alias of Contact-Link
                query.LinkEntities.Last().EntityAlias = entity.LogicalName;
                //
                //tracingService.Trace("FillLatestCompletedAppointmentFields(" + entity.LogicalName + ")", "Query has been built");

                // Retrieve appointment data
                var data = service.RetrieveMultiple(query);
                //
                //Log.Debug("FillLatestCompletedAppointmentFields(" + entity.LogicalName + ")", "Appointment data has been retrieved...");

                string dateField = "wid_lastsalesvisitdate";//.ToLowerInvariant();
                EntityReference parentcustomerId = new EntityReference();
                // appointmentTypeField = "wid_appointmenttype";//.ToLowerInvariant();

                // Did we find a completed appointment
                if (data != null && data.Entities.Count > 0)
                {
                    #region Handle case when we have a completed appointment
                    //
                    //Log.Debug("FillLatestCompletedAppointmentFields(" + entity.LogicalName + ")", "We have a completed appointment...");

                    var appointment = data.Entities.First().ToEntity<Entity>();

                    // Read the alias field of the 'wid_lastsalesvisitdate' from the related contact
                    var latestAppointmentDate = appointment.GetAttributeValue<AliasedValue>(entity.LogicalName + ".wid_lastsalesvisitdate");
                    if ("contact" == entity.LogicalName)
                    {
                        parentcustomerId = (EntityReference)appointment.GetAttributeValue<AliasedValue>(entity.LogicalName + ".parentcustomerid").Value;
                    }

                    // Do contact have a value in wid_lastsalesvisitdate?
                    if (latestAppointmentDate == null)
                    {
                        // No contact does not have a value in wid_lastsalesvisitdate!
                        // Does appointment have a value in ScheduledStart?
                        if (appointment.Contains("scheduledstart"))
                        // Appointment has a ScheduledStart; 
                        {
                            //if (((DateTime)appointment["scheduledstart"]).Date <= DateTime.Now.Date)//Dont update the last visit date with future date. Since this is taken care from appointment level[Plugin/JS validation to prevent the user from closing an appointment with future date] this part is commented.
                            entity[dateField] = appointment["scheduledstart"];
                        }
                    }
                    else
                    {
                        // Yes, contact has a value in wid_lastsalesvisitdate
                        var latestAppointmentDateValue = ((DateTime?)latestAppointmentDate.Value).GetValueOrDefault();
                        // If this is different from the ScheduledStart value update wid_lastsalesvisitdate
                        if (((DateTime?)appointment["scheduledstart"]).GetValueOrDefault(latestAppointmentDateValue) != latestAppointmentDateValue)
                        {
                            //if (((DateTime)appointment["scheduledstart"]).Date <= DateTime.Now.Date)//Dont update the last visit date with future date.
                            entity[dateField] = appointment["scheduledstart"];
                        }
                        else // this part needs to be tested thoroghly.
                        {
                            entity[dateField] = appointment["scheduledstart"];
                        }
                    }

                    #endregion
                }
                else
                {
                    #region Handle case when we don't have a completed appointment
                    //
                    //Log.Debug("FillLatestCompletedAppointmentFields(" + entity.LogicalName + ")", "We do NOT have a completed appointment; retrieving data from " + entity.LogicalName + " to se if we should nullify...");

                    // We did not find any open customer facing appointments for the current account/contact
                    // ... retrieve new_LatestAppointmentDate for the account/contact and when fields isn't null clear the fields

                    Entity currentEntity = null;

                    if ("contact" == entity.LogicalName)
                    {
                        currentEntity = service.Retrieve(
                            entity.LogicalName
                            , entity.Id
                            , new ColumnSet("contactid", "wid_lastsalesvisitdate", "parentcustomerid")//, "ap_LatestAppointmentTypeId")
                        );
                        parentcustomerId = (EntityReference)currentEntity["parentcustomerid"];
                    }
                    else if ("account" == entity.LogicalName)
                    {
                        currentEntity = service.Retrieve(
                            entity.LogicalName
                            , entity.Id
                            , new ColumnSet("accountid", "wid_lastsalesvisitdate")//, "ap_LatestAppointmentTypeId")
                        );
                    }

                    if (currentEntity.Contains(dateField))
                        entity[dateField] = null;

                    #endregion
                }

                if ("contact" == entity.LogicalName)
                {
                    entity["parentcustomerid"] = parentcustomerId;
                }
                // yield true whenever we have added attributes to the contact
                return currentNumberOfAttributesInContact != entity.Attributes.Count;
            }

            // We did not have a valid contact; thus, we havn't added/changed anything
            return false;
        }

        /// <summary>
        /// <para>Fills data into wid_nextopenappointment and/or wid_nextsalesvisitdate on a given account/contact.</para>
        /// <para>Data is taken from the next open customer facing appointment.</para>
        /// </summary>
        /// <param name="entity">The account/contact into which data should be filled.</param>
        /// <returns><c>true</c> when data has be filled into the account/contact; otherwise, <c>false</c></returns>
        private bool FillNextOpenAppointmentFields(Entity entity, IOrganizationService service)
        {
            // Ensure we have a contact with a valid ID
            if (entity != null && Guid.Empty != entity.Id)
            {
                //
                //Log.Debug("FillNextOpenAppointmentFields(" + entity.LogicalName + ")", "Searching for information and checking whether or not to fill data into " + entity.LogicalName + "...");

                // Store the current number of attributes in the contact
                var currentNumberOfAttributesInEntity = entity.Attributes.Count;
                var accountIdandContactIds = new List<string>();
                // Build query to retrieve data for the next open customer facing appointment 
                var query = new QueryExpression("appointment");
                query.ColumnSet.AddColumns("activityid", "scheduledstart");
                if (entity.LogicalName == "account")//if regardingobject is account then need to get all the associated contacts appointments
                {
                    accountIdandContactIds.Add(entity.Id.ToString());
                    //get all contact ids.
                    var QEcontact = new QueryExpression("contact");
                    // get contact ids
                    QEcontact.ColumnSet.AddColumns("contactid");
                    // Define filter QEcontact.Criteria
                    QEcontact.Criteria.AddCondition("parentcustomerid", ConditionOperator.Equal, entity.Id);
                    QEcontact.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);
                    EntityCollection contacts = service.RetrieveMultiple(QEcontact);
                    for (int i = 0; i < contacts.Entities.Count; i++)
                    {
                        accountIdandContactIds.Add(contacts.Entities[i].Id.ToString());//Add all the assoiciated contact Ids
                    }
                    query.Criteria.AddCondition("regardingobjectid", ConditionOperator.In, accountIdandContactIds.ToArray());//Here all the assoicated contact ids of account is passed along with account id
                }
                else
                {
                    query.Criteria.AddCondition("regardingobjectid", ConditionOperator.Equal, entity.Id.ToString());
                }
                query.Criteria.AddCondition("scheduledstart", ConditionOperator.GreaterEqual, DateTime.Now);//avoid past dates for open appointments.
                query.Criteria.AddFilter(new FilterExpression
                {
                    FilterOperator = LogicalOperator.Or,
                    Conditions =
                    {
                        new ConditionExpression("statecode", ConditionOperator.Equal, 0)
                        , new ConditionExpression("statecode", ConditionOperator.Equal, 3)
                    }
                });
                //query.AddLink("wid_appointmenttype", "wid_appointmenttypeid", "wid_appointmenttypeid") //as per the details in trello no need to check for faceto face
                //    .LinkCriteria.AddCondition("wid_facetoface", ConditionOperator.Equal, true);

                if (entity.LogicalName == "contact")
                    query.AddLink("contact", "regardingobjectid", "contactid")
                        .Columns.AddColumns("wid_nextopenappointment", "wid_nextsalesvisitdate");
                else if (entity.LogicalName == "account")//if it is account then we have to check for associated contacts as well.
                {
                    query.AddLink("account", "regardingobjectid", "accountid", JoinOperator.LeftOuter)
                        .Columns.AddColumns("wid_nextopenappointment", "wid_nextsalesvisitdate");
                    query.AddLink("contact", "regardingobjectid", "contactid", JoinOperator.LeftOuter)
                        .Columns.AddColumns("wid_nextopenappointment", "wid_nextsalesvisitdate");
                }

                query.AddOrder("scheduledstart", OrderType.Ascending);
                query.NoLock = true;
                query.Distinct = false;
                query.TopCount = 1;
                // Set alias of Contact-Link
                query.LinkEntities.Last().EntityAlias = entity.LogicalName;

                // Retrieve appointment data
                var data = service.RetrieveMultiple(query);

                // Define names on the fields to fill
                string dateField = "wid_nextsalesvisitdate";//.ToLowerInvariant();
                string lookupField = "wid_nextopenappointment";//.ToLowerInvariant();

                // Test: Did we find an open appointment
                if (data != null && data.Entities.Count > 0)
                {
                    //
                    //Log.Debug("Open appointments", String.Format("There is an open appointment for {0} with ID {1}", entity.LogicalName, entity.Id));

                    #region Handle case when we have an open appointment
                    // Yes, we have an open appointment
                    // ... cast to Appointment object
                    var appointment = data.Entities.First().ToEntity<Entity>();

                    // Read the alias field of the 'wid_nextsalesvisitdate' from the related account/contact
                    var nextOpenAppointmentDate = appointment.GetAttributeValue<AliasedValue>(entity.LogicalName + ".wid_nextsalesvisitdate");

                    // Do contact have a value in wid_nextsalesvisitdate?
                    if (nextOpenAppointmentDate == null)
                    {
                        // No contact does not have a value in wid_nextsalesvisitdate!
                        // Does appointment have a value in ScheduledStart?
                        if (appointment.Contains("scheduledstart"))
                            // Appointment has a ScheduledStart; set this as the wid_nextsalesvisitdate
                            entity[dateField] = appointment["scheduledstart"];
                    }
                    else
                    {
                        // Yes, contact has a value in wid_nextsalesvisitdate
                        var nextOpenAppointmentDateValue = ((DateTime?)nextOpenAppointmentDate.Value).GetValueOrDefault();
                        // If this is different from the ScheduledStart value update wid_nextsalesvisitdate
                        if (((DateTime?)appointment["scheduledstart"]).GetValueOrDefault(nextOpenAppointmentDateValue) != nextOpenAppointmentDateValue)
                            entity[dateField] = appointment["scheduledstart"];
                        else//this needs to be tested thoroughly
                        {
                            entity[dateField] = appointment["scheduledstart"];
                        }
                    }

                    // Read the alias field of the 'new_NextOpenAppointment' from the related account/contact
                    var nextOpenAppointment = appointment.GetAttributeValue<AliasedValue>(entity.LogicalName + ".wid_nextopenappointment");

                    // Do contact have a value in new_NextOpenAppointment?
                    if (nextOpenAppointment == null)
                    {
                        // No account/contact does not have a value in new_NextOpenAppointment; set the appointment as the new_NextOpenAppointment
                        entity[lookupField] = new EntityReference("appointment", appointment.Id);
                    }
                    else
                    {
                        // Yes, account/contact has a value in new_NextOpenAppointment
                        // If this is different from the current appointment; set the new_NextOpenAppointment to the current appointment
                        if ((nextOpenAppointment.Value as EntityReference).Id != appointment.Id)
                            entity[lookupField] = new EntityReference("appointment", appointment.Id);
                        else//this needs to be tested thoroughly
                        {
                            entity[lookupField] = new EntityReference("appointment", appointment.Id);
                        }
                    }
                    #endregion
                }
                else
                {
                    //
                    //Log.Debug("No open appointments", String.Format("There is no open appointments for {0} with ID {1}", entity.LogicalName, entity.Id));

                    #region Handle case when we don't have an open appointment
                    // We did not find any open customer facing appointments for the current contact
                    // ... retrieve new_NextOpenAppointment and new_NextOpenAppointmentDate for the contact and when fields isn't null
                    // clear the fields

                    Entity currentEntity = null;

                    if (entity.LogicalName == "contact")
                    {
                        currentEntity = service.Retrieve(
                            entity.LogicalName
                            , entity.Id
                            , new ColumnSet("contactid", "wid_nextopenappointment", "wid_nextsalesvisitdate")
                        );
                    }
                    else if (entity.LogicalName == "account")
                    {
                        currentEntity = service.Retrieve(
                            entity.LogicalName
                            , entity.Id
                            , new ColumnSet("accountid", "wid_nextopenappointment", "wid_nextsalesvisitdate")
                        );
                    }

                    if (currentEntity.Contains(lookupField))
                        entity[lookupField] = null;
                    if (currentEntity.Contains(dateField))
                        entity[dateField] = null;
                    #endregion
                }

                // yield true whenever we have added attributes to the contact
                return currentNumberOfAttributesInEntity != entity.Attributes.Count;
            }

            // We did not have a valid contact; thus, we havn't added/changed anything
            return false;
        }

    }
}
