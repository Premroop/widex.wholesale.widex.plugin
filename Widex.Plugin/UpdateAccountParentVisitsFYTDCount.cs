﻿using System;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;

namespace Widex.Plugin
{
    public class UpdateAccountParentVisitsFYTDCount : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService =
                (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)
                serviceProvider.GetService(typeof(IPluginExecutionContext));

            tracingService.Trace("Plugin started: Inside execute method");
            Entity entity = new Entity();
            // The InputParameters collection contains all the data passed 
            // in the message request.
            if (context.InputParameters.Contains("Target") &&
                context.InputParameters["Target"] is Entity)
            {
                // Obtain the target entity from the Input Parameters.
                tracingService.Trace
                    ("Getting the target entity from Input Parameters.");
                entity = (Entity)context.InputParameters["Target"];
            }
            else
            {
                if (context.InputParameters["Target"] is EntityReference)//In case of a delete operation
                {
                    entity.LogicalName = ((EntityReference)context.InputParameters["Target"]).LogicalName;
                    entity.Id = ((EntityReference)context.InputParameters["Target"]).Id;
                }
            }

            // Obtain the image entity from the Pre Entity Images.
            tracingService.Trace
                ("Getting image entity from PreEntityImages.");

            Entity preImage = context.PreEntityImages.Count > 0 ? context.PreEntityImages["PreImage"] : null;
            Entity postImage = context.PostEntityImages.Count > 0 ? (context.PostEntityImages.Contains("PostImage") ? context.PostEntityImages["PostImage"] : null) : null;

            // Verify that the target entity represents an account.
            // If not, this plug-in was not registered correctly.
            tracingService.Trace
                ("Verifying that the target entity represents an account.");
            if (entity.LogicalName != "account")
                return;

            try
            {
                tracingService.Trace("Entity Id:{0}", context.PrimaryEntityId);

                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = serviceFactory.CreateOrganizationService(null);//Running under the context of CRM system user
                IOrganizationService serviceUserContext = serviceFactory.CreateOrganizationService(context.InitiatingUserId);

                UpdateParentVisitsFYTDCount(entity, preImage, postImage, service, serviceUserContext);

                tracingService.Trace("Plugin execution completed.");
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                tracingService.Trace("The application terminated with an error.");
                tracingService.Trace("Timestamp: {0}", ex.Detail.Timestamp);
                tracingService.Trace("Code: {0}", ex.Detail.ErrorCode);
                tracingService.Trace("Message: {0}", ex.Detail.Message);
                tracingService.Trace("Inner Fault: {0}",
                    null == ex.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault");
            }
            catch (System.TimeoutException ex)
            {
                tracingService.Trace("The application terminated with an error.");
                tracingService.Trace("Message: {0}", ex.Message);
                tracingService.Trace("Stack Trace: {0}", ex.StackTrace);
                tracingService.Trace("Inner Fault: {0}",
                    null == ex.InnerException.Message ? "No Inner Fault" : ex.InnerException.Message);
            }
            catch (InvalidPluginExecutionException ex)
            {
                throw ex;
            }
            catch (System.Exception ex)
            {
                tracingService.Trace("The application terminated with an error.");
                tracingService.Trace(ex.Message);

                // Display the details of the inner exception.
                if (ex.InnerException != null)
                {
                    tracingService.Trace(ex.InnerException.Message);

                    FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> fe = ex.InnerException
                        as FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>;
                    if (fe != null)
                    {
                        tracingService.Trace("Timestamp: {0}", fe.Detail.Timestamp);
                        tracingService.Trace("Code: {0}", fe.Detail.ErrorCode);
                        tracingService.Trace("Message: {0}", fe.Detail.Message);
                        tracingService.Trace("Trace: {0}", fe.Detail.TraceText);
                        tracingService.Trace("Inner Fault: {0}",
                            null == fe.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault");
                    }
                }
            }
        }

        private void UpdateParentVisitsFYTDCount(Entity entity, Entity preImage, Entity postImage, IOrganizationService service, IOrganizationService serviceUserContext)
        {
            //If parent account lookup is blank then the account is parent account

            Guid parentAccountId = postImage.Contains("parentaccountid") && postImage["parentaccountid"] != null ? ((EntityReference)postImage["parentaccountid"]).Id : entity.Id;

            var fetchXml = $@"
            <fetch>
              <entity name='account'>
                <attribute name='wid_actualsalesvisits_fytd' />
                <attribute name='wid_parentvisitsfytd' />
                <attribute name='accountid' />
                <filter type='or'>
                  <condition attribute='accountid' operator='eq' value='{parentAccountId/*5772175e-8208-46fe-9541-ae9567c28d17*/}'/>
                  <condition attribute='parentaccountid' operator='eq' value='{parentAccountId/*345a7e13-54d7-4411-970d-f29c26f888d6*/}'/>
                </filter>
                <filter>
                  <condition attribute='statecode' operator='eq' value='0' />
                </filter>
              </entity>
            </fetch>";

            EntityCollection accounts = service.RetrieveMultiple(new FetchExpression(fetchXml));
            int sumOfVisitsFYTD = 0;

            foreach (var account in accounts.Entities)
            {
                if (account.Contains("wid_actualsalesvisits_fytd"))
                {
                    sumOfVisitsFYTD = sumOfVisitsFYTD + (int)account["wid_actualsalesvisits_fytd"];
                }
            }

            var requestWithResults = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = false
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

            foreach (var account in accounts.Entities)
            {
                if (account.Contains("wid_actualsalesvisits_fytd"))
                {
                    account.Attributes.Remove("wid_actualsalesvisits_fytd");
                }
                //Perform a validation here. Get wid_parentvisitsfytd and compare with sumOfVisitsFYTD. Update account only if the value is different.

                UpdateRequest updateRequest = new UpdateRequest { Target = account };

                if (account.Contains("wid_parentvisitsfytd"))
                {
                    if (sumOfVisitsFYTD != (int)account["wid_parentvisitsfytd"])
                    {
                        account["wid_parentvisitsfytd"] = sumOfVisitsFYTD;
                        requestWithResults.Requests.Add(updateRequest);
                    }
                }
                else
                {
                    account["wid_parentvisitsfytd"] = sumOfVisitsFYTD;
                    requestWithResults.Requests.Add(updateRequest);
                }

                if (requestWithResults.Requests.Count == 1000)
                {
                    ExecuteMultipleAccount(service, requestWithResults);
                    requestWithResults.Requests.Clear();
                }
            }

            ExecuteMultipleAccount(service, requestWithResults);
        }

        private void ExecuteMultipleAccount(IOrganizationService service, ExecuteMultipleRequest requestWithResults)
        {
            ExecuteMultipleResponse responseWithNoResults =
                   (ExecuteMultipleResponse)service.Execute(requestWithResults);

            // There should be no responses unless there was an error. 
            if (responseWithNoResults.Responses.Count > 0)
            {
                foreach (var responseItem in responseWithNoResults.Responses)
                {
                    if (responseItem.Fault != null)
                        throw new InvalidPluginExecutionException(responseItem.Fault.Message);
                }
            }
        }
    }
}
